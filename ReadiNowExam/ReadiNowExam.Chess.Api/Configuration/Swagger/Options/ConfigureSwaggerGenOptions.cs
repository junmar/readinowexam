﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ReadiNowExam.Chess.Api.Configuration.Swagger.Options
{
    public class ConfigureSwaggerGenOptions : IConfigureOptions<SwaggerGenOptions>
    {
        private readonly IConfiguration _configuration;
        private readonly IApiVersionDescriptionProvider _apiVersionDescriptionProvider;

        public ConfigureSwaggerGenOptions(
            IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public ConfigureSwaggerGenOptions(
            IConfiguration configuration,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _apiVersionDescriptionProvider = apiVersionDescriptionProvider ?? throw new ArgumentNullException(nameof(apiVersionDescriptionProvider));
        }

        public void Configure(SwaggerGenOptions options)
        {
            // Add a swagger document for each api version defined
            var versions = _apiVersionDescriptionProvider?.ApiVersionDescriptions.Select(x => x.GroupName).ToList()
                           ?? new List<string>() { "v1.0" };

            foreach (var version in versions)
            {
                options.SwaggerDoc(
                    version,
                    new OpenApiInfo()
                    {
                        Title = _configuration["Api:Swagger:ApplicationName"],
                        Version = version
                    });
            }
        }
    }
}
