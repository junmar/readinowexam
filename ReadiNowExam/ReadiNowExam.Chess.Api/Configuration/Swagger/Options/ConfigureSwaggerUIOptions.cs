﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace ReadiNowExam.Chess.Api.Configuration.Swagger.Options
{
    public class ConfigureSwaggerUIOptions : IConfigureOptions<SwaggerUIOptions>
    {
        private readonly IConfiguration _configuration;
        private readonly IApiVersionDescriptionProvider _apiVersionDescriptionProvider;

        public ConfigureSwaggerUIOptions(
            IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public ConfigureSwaggerUIOptions(
            IConfiguration configuration,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _apiVersionDescriptionProvider = apiVersionDescriptionProvider ?? throw new ArgumentNullException(nameof(apiVersionDescriptionProvider));
        }

        public void Configure(SwaggerUIOptions options)
        {
            // Add a swagger endpoint for each api version defined
            var versions = _apiVersionDescriptionProvider?.ApiVersionDescriptions.Select(x => x.GroupName).ToList()
                ?? new List<string>() { "v1.0" };

            foreach (var version in versions)
            {
                options.SwaggerEndpoint($"/swagger/{version}/swagger.json", version);
            }

            // Disable document expansion by default
            options.DocExpansion(DocExpansion.None);

        }
    }
}
