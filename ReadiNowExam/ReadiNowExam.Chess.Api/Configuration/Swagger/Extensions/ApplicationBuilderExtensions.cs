﻿using Microsoft.AspNetCore.Builder;

namespace ReadiNowExam.Chess.Api.Configuration.Swagger.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseChessSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();

            return app;
        }

        public static IApplicationBuilder UseChessSwaggerUI(this IApplicationBuilder app)
        {
            app.UseSwaggerUI();

            return app;
        }
    }
}
