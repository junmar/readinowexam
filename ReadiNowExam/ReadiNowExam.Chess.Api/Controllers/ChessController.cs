﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReadiNowExam.Chess.Api.Models;
using ReadiNowExam.Chess.Managers;
using ReadiNowExam.Chess.Managers.Models;

namespace ReadiNowExam.Chess.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChessController
    {
        private readonly ILogger<ChessController> _logger;
        private readonly IMapper _mapper;
        private readonly ICreateGameManager _createGameManager;
        private readonly IMoveManager _moveManager;
        public ChessController(
            ILogger<ChessController> logger,
            IMapper mapper,
            ICreateGameManager createGameManager,
            IMoveManager moveManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _createGameManager = createGameManager ?? throw new ArgumentNullException(nameof(createGameManager));
            _moveManager = moveManager ?? throw new ArgumentNullException(nameof(moveManager));
        }

        [HttpPost("new")]
        public async Task<ActionResult<NewGameResultModel>> CreateNewGameAsync(NewGameRequestModel requestModel)
        {
            _logger.LogTrace("CreateNewGameAsync started");
            var createModel = _mapper.Map<CreateChessGameModel>(requestModel);
            var result = await _createGameManager.HandleAsync(createModel);

            _logger.LogTrace("CreateNewGameAsync finished.");

            return _mapper.Map<NewGameResultModel>(result);
        }

        [HttpPost("move")]
        public async Task<ActionResult<NewGameResultModel>> MoveAsync(MoveRequestModel requestModel)
        {
            _logger.LogTrace("MoveAsync started");

            var moveModel = _mapper.Map<ChessMoveModel>(requestModel);
            var result = await _moveManager.HandleAsync(moveModel);

            _logger.LogTrace("MoveAsync finished.");

            return _mapper.Map<NewGameResultModel>(result);
        }
    }
}
