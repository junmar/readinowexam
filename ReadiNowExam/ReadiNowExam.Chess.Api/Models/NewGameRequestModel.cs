﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadiNowExam.Chess.Api.Models
{
    public class NewGameRequestModel
    {
        public NewGameRequestModelPlayer BlackPlayer { get; set; }
        public NewGameRequestModelPlayer WhitePlayer { get; set; }
        public class NewGameRequestModelPlayer
        {
            public string Name { get; set; }
            public string Email { get; set; }
        }
    }
}
