﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ReadiNowExam.Chess.Common.Enums;

namespace ReadiNowExam.Chess.Api.Models
{
    public class NewGameResultModel
    {
        public int GameId { get; set; }

        public int WhitePlayerId { get; set; }

        public int BlackPlayerId { get; set; }

        public DateTime Started { get; set; }

        public DateTime? Ended { get; set; }

        public NewGameResultModelBoard Board { get; set; }
        public NewGameResultModelPlayer WhitePlayer { get; set; }
        public NewGameResultModelPlayer BlackPlayer { get; set; }

        /// <summary>
        /// The <see cref="Player"/>.
        /// </summary>
        public class NewGameResultModelPlayer
        {
            /// <summary>
            /// Gets or sets the player id.
            /// </summary>
            public int PlayerId { get; set; }

            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the email.
            /// </summary>
            public string Email { get; set; }

            public DateTime CreatedDate { get; set; }
        }
        public class NewGameResultModelBoard
        {
            public int ChessBoardId { get; set; }

            /// <summary>
            /// Gets or sets the game id.
            /// </summary>
            public int GameId { get; set; }
            /// <summary>
            /// Gets or sets the grid.
            /// </summary>
            public List<NewGameResultModelLightSquareModel> Grid { get; set; }
        }

        public class NewGameResultModelLightSquareModel
        {
            /// <summary>
            /// Gets or sets the square id.
            /// </summary>
            public int LightSquareId { get; set; }

            /// <summary>
            /// Gets sets the chessboard id.
            /// </summary>
            /// 
            public int ChessBoardId { get; set; }
            /// <summary>
            /// Gets or sets the row number.
            /// </summary>
            public int RowNumber { get; set; }

            /// <summary>
            /// Gets or sets the column number.
            /// </summary>
            public int ColumnNumber { get; set; }

            /// <summary>
            /// Value indicates that whether or not the square is occupied.
            /// </summary>
            public bool IsOccupied => ChessPieceId.HasValue;

            public int? ChessPieceId { get; set; }

            public ColorEnum Color { get; set; }
        }
    }
}
