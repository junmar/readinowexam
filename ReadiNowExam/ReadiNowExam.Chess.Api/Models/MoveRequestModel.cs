﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadiNowExam.Chess.Api.Models
{
    public class MoveRequestModel
    {
        public int GameId { get; set; }

        public int PlayerId { get; set; }

        public int ChessPieceId { get; set; }

        public int StartLocation { get; set; }

        public int EndLocation { get; set; }

        public DateTime Date { get; set; }
    }
}
