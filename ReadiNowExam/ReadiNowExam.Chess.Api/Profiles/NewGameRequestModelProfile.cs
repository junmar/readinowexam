﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ReadiNowExam.Chess.Api.Models;
using ReadiNowExam.Chess.Managers;

namespace ReadiNowExam.Chess.Api.Profiles
{
    public class NewGameRequestModelProfile : Profile
    {
        public NewGameRequestModelProfile()
        {
            CreateMap<NewGameRequestModel, CreateChessGameModel>();

            CreateMap<NewGameRequestModel.NewGameRequestModelPlayer, CreateChessGameModel.CreateChessGameModelPlayer>();

        }
    }
}
