﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ReadiNowExam.Chess.Api.Models;
using ReadiNowExam.Chess.Managers.Models;

namespace ReadiNowExam.Chess.Api.Profiles
{
    public class MoveRequestModelProfile : Profile
    {
        public MoveRequestModelProfile()
        {
            CreateMap<MoveRequestModel, ChessMoveModel>();
        }
    }
}

