﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ReadiNowExam.Chess.Api.Models;
using ReadiNowExam.Chess.Models;

namespace ReadiNowExam.Chess.Api.Profiles
{
    public class NewGameResultModelProfile : Profile
    {
        public NewGameResultModelProfile()
        {
            CreateMap<ChessGame, NewGameResultModel>()
                .ForMember(d => d.WhitePlayer, m => m.MapFrom(s => s.WhitePlayer))
                .ForMember(d => d.BlackPlayer, m => m.MapFrom(s => s.BlackPlyer));

            CreateMap<ChessBoard, NewGameResultModel.NewGameResultModelBoard>();

            CreateMap<LightSquare, NewGameResultModel.NewGameResultModelLightSquareModel>();

            CreateMap<Player, NewGameResultModel.NewGameResultModelPlayer>();
        }
    }
}
