
select p.Name as [Player Name], Count(*) as [Count of Moves] from Player p inner join Game g on p.PlayerId = g.WhitePlayerId
inner join Game g1 on p.PlayerId = g1.BlackPlayerid
inner join [Move] m on g.GameId = m.GameId
inner join  [Move] m1 on g1.GameId = m1.GameId
 where (year(m.Date) = 2022 and MONTH(m.Date) = 1) or (year(m1.Date) = 2022 and MONTH(m1.Date) = 1)
 group by p.Name