﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ReadiNowExam.Chess.Managers.Models;
using ReadiNowExam.Chess.Models;

namespace ReadiNowExam.Chess.Managers
{
    public interface IMoveManager
    {
        Task<ChessGame> HandleAsync(ChessMoveModel model);
    }
}
