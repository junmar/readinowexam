﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ReadiNowExam.Chess.Models;

namespace ReadiNowExam.Chess.Managers
{
    public interface ICreateGameManager
    {
        Task<ChessGame> HandleAsync(CreateChessGameModel model);

    }
}
