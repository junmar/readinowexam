﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace ReadiNowExam.Chess.Managers.Extensions
{
    public static class  ServiceCollectionExtension
    {
        public static IServiceCollection AddManagers(this IServiceCollection services)
        {
            services.AddScoped<ICreateGameManager, CreateGameManager>();
            services.AddScoped<IMoveManager, MoveManager>();
            return services;
        }
    }
}
