﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReadiNowExam.Chess.Managers.Models
{
    public class ChessMoveModel
    {
        public int GameId { get; set; }

        public int PlayerId { get; set; }

        public int ChessPieceId { get; set; }

        public int StartLocation { get; set; }

        public int EndLocation { get; set; }

        public DateTime Date { get; set; }
    }
}
