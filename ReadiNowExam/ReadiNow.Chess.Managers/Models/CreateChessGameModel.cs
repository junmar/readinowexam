﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReadiNowExam.Chess.Managers
{
    public class CreateChessGameModel
    {
        public CreateChessGameModelPlayer BlackPlayer { get; set; }
        public CreateChessGameModelPlayer WhitePlayer { get; set; }
        public class CreateChessGameModelPlayer
        {
            public string Name { get; set; }
            public string Email { get; set; }
        }
    }
}
