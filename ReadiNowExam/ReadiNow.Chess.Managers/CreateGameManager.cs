﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ReadiNowExam.Chess.Models;

namespace ReadiNowExam.Chess.Managers
{
    public class CreateGameManager : ICreateGameManager
    {
        public async Task<ChessGame> HandleAsync(CreateChessGameModel model)
        {
            return new ChessGame
            {
                GameId = 1,
                BlackPlayerId = 1,
                WhitePlayerId = 2,
                Started = DateTime.Now,
                Board = new ChessBoard(),
                BlackPlyer = new Player
                {
                    PlayerId = 1,
                   Email = model.BlackPlayer.Name,
                   Name = model.BlackPlayer.Email
                },
                WhitePlayer = new Player
                {
                    PlayerId = 2,
                    Email = model.WhitePlayer.Email,
                    Name = model.WhitePlayer.Name
                }
            };
        }
    }
}
