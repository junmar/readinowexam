﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ReadiNowExam.Chess.Managers.Models;
using ReadiNowExam.Chess.Models;

namespace ReadiNowExam.Chess.Managers
{
    public class MoveManager : IMoveManager
    {
        public Task<ChessGame> HandleAsync(ChessMoveModel model)
        {
            // assuming the move was done and the object state was updated.
            var game = new ChessGame
            {
                GameId = 1,
                BlackPlayerId = 1,
                WhitePlayerId = 2,
                Started = DateTime.Now,
                Board = new ChessBoard(),
            };
            game.Board.Grid[model.EndLocation].ChessPieceId = model.ChessPieceId;

            return Task.FromResult(game);
        }
    }
}
