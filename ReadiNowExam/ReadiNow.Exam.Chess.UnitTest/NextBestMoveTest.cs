using System;
using NUnit.Framework;
using ReadiNowExam.Chess.Models;

namespace ReadiNow.Exam.Chess.UnitTest
{
    public class NextBestMoveTest
    {
        private NextBestMove sut;
        private IWorkQueue _workQueue;
        [SetUp]
        public void Setup()
        {
            _workQueue = new WorkQueue();

            sut = new NextBestMove(_workQueue);
        }

        [Test]
        public void CalculateNextBestMove_ThrowsException_WhenPlayerHasNoPaidSubscription()
        {
            var chessGame = new ChessGame();
            chessGame.IsCompetition = false;
            var player = new Player();
            player.HasSubscription = false;
            
            var ex = Assert.Throws<Exception>(() => sut.CalculateNextBestMove(chessGame, player));

            Assert.That(ex.Message, Is.EqualTo("This feature requires a subscription"));
        }

        [Test]
        public void CalculateNextBestMove_ThrowsException_WhenGameIsCompitition()
        {
            var chessGame = new ChessGame();
            chessGame.IsCompetition = true;
            var player = new Player();
            var ex = Assert.Throws<Exception>(() => sut.CalculateNextBestMove(chessGame,player));

            Assert.That(ex.Message, Is.EqualTo("This feature is disabled for competition games"));
        }

        [Test]
        public void CalculateNextBestMove_ThrowsException_WhenQueueLimitIsReached()
        {
            for(var i = 0; i<=100;i++)
            {
                _workQueue.Enqueue(new ReadiNowExam.Chess.Model.NextBestMoveRequest());
            }

            var chessGame = new ChessGame();
            chessGame.IsCompetition = false;
            var player = new Player();
            player.HasSubscription = true;

            var ex = Assert.Throws<Exception>(() => sut.CalculateNextBestMove(chessGame, player));

            Assert.That(ex.Message, Is.EqualTo("Feature unavailable at this time. Please try again later"));
        }
        [Test]
        public void CalculateNextBestMove_ExecutesExpectedLogic()
        {
            var chessGame = new ChessGame();
            chessGame.IsCompetition = false;
            var player = new Player();
            player.HasSubscription = true;

            sut.CalculateNextBestMove(chessGame, player);

            Assert.That(_workQueue.QueueLength, Is.EqualTo(1));
        }
    }
}