﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Common.Enums;
using ReadiNowExam.Chess.Models;

namespace ReadiNowExam.Chess.Model
{
    public class NextBestMoveRequest
    {
        public ChessBoard Board { get; set; }
        public ColorEnum Color { get; set; }
    }
}
