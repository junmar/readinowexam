﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Model;

namespace ReadiNowExam.Chess.Models
{
    public class NextBestMove
    {
        private const int MaxQueueLength = 100;
        private readonly IWorkQueue _queue;

        public NextBestMove(IWorkQueue queue)
        {
            _queue = queue;
        }

        public void CalculateNextBestMove(ChessGame game, Player player)
        {
            if (game.IsCompetition)
            {
                throw new Exception("This feature is disabled for competition games");
            }

            if (!player.HasSubscription)
            {
                throw new Exception("This feature requires a subscription");
            }

            if (_queue.QueueLength > MaxQueueLength)
            {
                throw new Exception("Feature unavailable at this time. Please try again later");
            }

            var request = new NextBestMoveRequest
            {
                Board = game.Board,
                Color = player.Color
            };

            _queue.Enqueue(request);
        }
    }
}
