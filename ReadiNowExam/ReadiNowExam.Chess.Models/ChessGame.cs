﻿using System;

namespace ReadiNowExam.Chess.Models
{
    public class ChessGame
    {
        public int GameId { get; set; }

        public int WhitePlayerId { get; set; }

        public int BlackPlayerId { get; set; }

        public DateTime Started { get; set; }

        public DateTime? Ended { get; set; }

        public virtual ChessBoard Board { get; set; }

        public virtual Player WhitePlayer { get; set; }

        public virtual Player BlackPlyer { get; set; }

        public bool IsCompetition { get; set; }
    }
}
