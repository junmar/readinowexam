﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Common.Enums;

namespace ReadiNowExam.Chess.Models
{
    /// <summary>
    /// The <see cref="Player"/>.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool HasSubscription { get; set; }

        public ColorEnum Color { get; set; }
    }
}
