﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Model;

namespace ReadiNowExam.Chess.Models
{
    public  interface IWorkQueue
    {
        int QueueLength { get; }
        void Enqueue(NextBestMoveRequest request);
        bool TryDequeue(out NextBestMoveRequest request);
    }
}
