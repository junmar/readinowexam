﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Common.Enums;

namespace ReadiNowExam.Chess.Models
{
    /// <summary>
    /// The <see cref="ChessBoard"/>
    /// </summary>
    public class ChessBoard
    {
        /// <summary>
        /// Gets or sets the board id.
        /// </summary>
        public int ChessBoardId { get; set; }

        /// <summary>
        /// Gets or sets the game id.
        /// </summary>
        public int GameId { get; set; }
        /// <summary>
        /// Gets or sets the grid.
        /// </summary>
        public List<LightSquare> Grid { get; set; }

        public ChessBoard()
        {
            Grid = new List<LightSquare>();
            int ctr = 1;
            var currColor = ColorEnum.Black;
            for (int i = 0; i < 8; i++)
            {
                for (int x = 0; x < 8; x++)
                {
                    var square = new LightSquare(i, x, currColor);
                    square.LightSquareId = ctr;
                    Grid.Add(square);
                    currColor = currColor == ColorEnum.Black ? ColorEnum.White : ColorEnum.Black;
                    ctr++;
                }
            }
        }
    
    }
}
