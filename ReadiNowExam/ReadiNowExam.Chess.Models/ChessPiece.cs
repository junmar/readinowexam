﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Common.Enums;

namespace ReadiNowExam.Chess.Models
{
    /// <summary>
    /// The <see cref="ChessPiece"/>
    /// </summary>
    public class ChessPiece
    {
        /// <summary>
        /// Gets or sets the piece id.
        /// </summary>
        public int ChessPieceId { get; set; }
        /// <summary>
        /// Gets or sets the piece type
        /// </summary>
        public PieceType PieceType { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// Value indicating if piece is killed or now.
        /// </summary>
        public bool IsKilled { get; set; }

    }
}
