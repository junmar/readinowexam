﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Common.Enums;

namespace ReadiNowExam.Chess.Models
{
    /// <summary>
    /// The <see cref="LightSquare"/>
    /// </summary>
    public class LightSquare
    {
        /// <summary>
        /// Gets or sets the square id.
        /// </summary>
        public int LightSquareId { get; set; }

        /// <summary>
        /// Gets sets the chessboard id.
        /// </summary>
        /// 
        public int ChessBoardId { get; set; }
        /// <summary>
        /// Gets or sets the row number.
        /// </summary>
        public int RowNumber { get; set; }

        /// <summary>
        /// Gets or sets the column number.
        /// </summary>
        public int ColumnNumber { get; set; }

        /// <summary>
        /// Value indicates that whether or not the square is occupied.
        /// </summary>
        public bool IsOccupied => ChessPieceId.HasValue;

        public int? ChessPieceId  { get; set; }

        public ColorEnum Color { get; set; }

        public LightSquare(int x, int y, ColorEnum color)
        {
            RowNumber = x;
            ColumnNumber = y;
            Color = color;
        }

        public virtual ChessBoard Board { get; set; }
    }
}
