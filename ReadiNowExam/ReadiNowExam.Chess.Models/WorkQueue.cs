﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Model;

namespace ReadiNowExam.Chess.Models
{
    public class WorkQueue : IWorkQueue
    {
        ConcurrentQueue<NextBestMoveRequest> queue;
        object syncLock = new object();
        public WorkQueue()
        {
            queue = new ConcurrentQueue<NextBestMoveRequest>();
        }

        public int QueueLength => queue.Count;

        public void Enqueue(NextBestMoveRequest request)
        {
            lock (syncLock)
            {
                queue.Enqueue(request);
            }
        }

        public bool TryDequeue(out NextBestMoveRequest request)
        {
            lock (syncLock)
            {
                return queue.TryDequeue(out request);
            }
        }
    }
}
