﻿using System;
using System.Collections.Generic;
using System.Text;
using ReadiNowExam.Chess.Common.Enums;

namespace ReadiNowExam.Chess.Models
{
    public class ChessMove
    {
        public int MoveId { get; set; }

        public int GameId { get; set; }

        public ColorEnum PlayerColor { get; set; }

        public string Piece { get; set; }

        public int StartPosition { get; set; }

        public int EndPosition { get; set; }

        public DateTime Date { get; set; }

    }
}
