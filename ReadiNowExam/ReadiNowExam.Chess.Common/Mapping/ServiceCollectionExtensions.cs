﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace ReadiNowExam.Chess.Common.Mapping
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddChessAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(
                (serviceProvider, mapperConfiguration) =>
                {
                    // Read all AutoMapper profiles from the service provider
                    var profiles = serviceProvider.GetServices<Profile>();

                    // Add all profiles to AutoMapper
                    mapperConfiguration.AddProfiles(profiles);

                    // Configure AutoMapper to use the Microsoft DI
                    mapperConfiguration.ConstructServicesUsing(type => ActivatorUtilities.CreateInstance(serviceProvider, type));
                },
                Array.Empty<Assembly>());

            return services;
        }

        public static IServiceCollection AddAutoMapperProfilesFromAssembly(this IServiceCollection services, Assembly assemblyToScan)
        {
            services.Scan(s =>
            {
                s.FromAssemblies(assemblyToScan)
                    .AddClasses(c => c.AssignableTo<Profile>())
                    .As<Profile>()
                    .WithTransientLifetime();
            });

            return services;
        }
    }
}
