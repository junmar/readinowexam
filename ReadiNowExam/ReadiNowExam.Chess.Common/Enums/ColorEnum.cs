﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReadiNowExam.Chess.Common.Enums
{
    public enum ColorEnum
    {
        White = 0,
        Black = 1
    }
}
