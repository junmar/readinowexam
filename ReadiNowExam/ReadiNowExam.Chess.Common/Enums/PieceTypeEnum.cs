﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReadiNowExam.Chess.Common.Enums
{
    public enum PieceType
    {
        BlackPawn,
        WhitePawn,
        BlackKnight,
        WhiteKnight,
        BlackBishop,
        WhiteBishop,
        BlackRook,
        WhiteRook,
        BlackQueen,
        WhiteQueen,
        BlackKing,
        WhiteKing,
    }
}
